package com.kolomiychuk.taxi;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Наталия on 15.04.2017.
 */
@Controller // говорим spring контейнеру чтобы он создал этот бин
public class IndexController {
    @RequestMapping(
            value = "/" /*путь по которому будет вызываться метод этого контроллера*/,
            method = RequestMethod.GET /* http метод*/)
    public String printHello(Model model) {
        model.addAttribute("message", "Hello Spring MVC Framework!"); // данные которые мы передаем на веб-страницу
        return "index"; // название нашей веб страницы, которая лежит в WEB-INF/pages
    }
}
