package com.kolomiychuk.taxi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * Created by Наталия on 15.04.2017.
 */
@Configuration // говорим спрингу что у нас это класс с его конфигурацией
@EnableWebMvc
@ComponentScan("com.kolomiychuk.taxi") // говорим спрингу корневой пакет с которого он начнет искать файлы конфигурации и бины
public class WebAppConfig extends WebMvcConfigurerAdapter {

    // Позволяет видеть все ресурсы в папках styles и scripts, такие как картинки, стили и т.п.
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/styles/**").addResourceLocations("/styles/");
        registry.addResourceHandler("/scripts/**").addResourceLocations("/scripts/");
    }

    // а этот бин инициализирует View нашего проекта
    // тут мы указываем префикс и суфикс к нашим веб-страницам
    @Bean
    public InternalResourceViewResolver setupViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/pages/");
        resolver.setSuffix(".jsp");
        resolver.setViewClass(JstlView.class);
        return resolver;
    }
}